package ro.mta.se.chat.utils;

/**
 * Created by ElenaPC on 28-Oct-15.
 */
public class Constants {
    public final static int LOGGING_LEVEL = 5;
    public static final boolean LOG_TO_FILE = true; //afisare in fisier
    public static final String FileName = "Fisier.txt"; //denumire fisier
}
