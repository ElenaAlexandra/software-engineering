package ro.mta.se.chat.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by ElenaPC on 01-Nov-15.
 */
public class LoggerElena {

    private LoggerElena(){

    }

    public static final boolean LOG_TO_FILE = true;

    public static void log(int level,String Mesaj, Exception e)
    {


        try {
            FileOutputStream fOut = new FileOutputStream("file.txt");
            PrintStream ps = new PrintStream(fOut);

            if (LOG_TO_FILE == true) {
                ps.println("Level :" + level);
                ps.println(Mesaj);
                ps.println(e.getMessage());
            } else {
                System.out.println("Level :" + level);
                System.out.println(Mesaj);
                System.out.println(e.getMessage());
            }

            ps.close();
            fOut.close();
        }
            catch(IOException exceptie)
            {
                System.out.println("Could not create file");
                System.exit(1);

            }
    }
    public static void log(int level, String Mesaj){

        try {
            FileOutputStream fOut = new FileOutputStream("file.txt");
            PrintStream ps = new PrintStream(fOut);

            if (LOG_TO_FILE == true) {
                ps.println("Level :" + level);
                ps.println(Mesaj);
            } else {
                System.out.println("Level :" + level);
                System.out.println(Mesaj);
            }

            ps.close();
            fOut.close();
        }
        catch(IOException exceptie)
        {
            System.out.println("Could not create file");
            System.exit(1);

        }
    }
    public static void log(int level, String Mesaj, String stackTrace){

        try {
            FileOutputStream fOut = new FileOutputStream("file.txt");
            PrintStream ps = new PrintStream(fOut);

            if (LOG_TO_FILE == true) {
                ps.println("Level :" + level);
                ps.println(Mesaj);
                ps.println("StackTrace: "+stackTrace);
            } else {

                System.out.println("Level :"+level);
                System.out.println(Mesaj);
                System.out.println("StackTrace: "+stackTrace);
            }

            ps.close();
            fOut.close();
        }
        catch(IOException exceptie)
        {
            System.out.println("Could not create file");
            System.exit(1);

        }

    }
    public static void log(int level, String Mesaj, int Error){

        try {
            FileOutputStream fOut = new FileOutputStream("file.txt");
            PrintStream ps = new PrintStream(fOut);

            if (LOG_TO_FILE == true) {
                ps.println("Level :" + level);
                ps.println(Mesaj);
                ps.println("Error code:"+Error);
            } else {
                System.out.println("Level :"+level);
                System.out.println(Mesaj);
                System.out.println("Error code:"+Error);
            }

            ps.close();
            fOut.close();
        }
        catch(IOException exceptie)
        {
            System.out.println("Could not create file");
            System.exit(1);

        }

    }

}
