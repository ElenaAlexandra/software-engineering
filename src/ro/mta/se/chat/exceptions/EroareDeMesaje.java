package ro.mta.se.chat.exceptions;

import java.io.IOException;

/**
 * Created by ElenaPC on 01-Nov-15.
 */
public class EroareDeMesaje extends Exception {

    String Mesaj=null;
    public EroareDeMesaje(String M){
        super(M);
        Mesaj=M;
    }
    public EroareDeMesaje(IOException e){
        super(e);
        Mesaj=e.getMessage();
    }

    public String ToString(){
        Mesaj="Eroare De Mesaje"+ Mesaj;
        return Mesaj;
    }

}
