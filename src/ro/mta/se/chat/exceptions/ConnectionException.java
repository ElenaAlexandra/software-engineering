package ro.mta.se.chat.exceptions;

import java.io.IOException;

/**
 * Created by ElenaPC on 01-Nov-15.
 */
public class ConnectionException extends Exception
{

    private String Mesaj;

    public ConnectionException(String Mes){
        super(Mes);
        this.Mesaj=Mes;
    }
    public ConnectionException(Throwable cauza){
        super(cauza);
        Mesaj=cauza.getMessage();
    }

    public ConnectionException(Exception e){
        super(e);
        Mesaj=e.getMessage();
    }
    public ConnectionException(IOException ee){
        super(ee);
        Mesaj=ee.getMessage();
    }

    public String GiveMeTheMesage(){
        return (">>Mesaj de Eroare Personalizat<<"+Mesaj);
    }


}
