package ro.mta.se.chat.exceptions;

/**
 * Created by ElenaPC on 01-Nov-15.
 */
import java.io.IOException;
import java.net.*;

public class Server {
    ServerSocket server=null;
    Socket socket=null;

    public void Connect() throws ConnectionException{

        try{

            server=new ServerSocket(3333);
            server.setSoTimeout(200);
            socket=server.accept();
        }
        catch (IOException e){
            throw new ConnectionException("Eraore de Conexiune");
        }
    }
}
